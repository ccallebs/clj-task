(ns clj-task.core)

(require '[clojure.string :as string])
(require '[clojure.java.io :as io])

(defn add-task [options]
  (spit ".clj-task/tasks/1.task" options))

(defn remove-task [options]
  (io/delete-file ".clj-task/tasks/1.task"))

(defn show-task [task-id]
  (println (slurp ".clj-task/tasks/1.task")))

(defn print-help []
  (println "HELP DOCUMENTATION SHOULD GO HERE."))

(defn run [command options]
  (cond (= command "add")  (add-task options)
        (= command "rm")   (remove-task options)
        (= command "show") (show-task options)
        :else             (print-help)))

(defn fetch-command [args]
  (nth args 0))

(defn fetch-options [args]
  (next args))

(defn setup-file-structure []
  (let [tasks-dir ".clj-task/tasks"]
  (.mkdir (java.io.File. tasks-dir))))

(defn -main [& args]
  (setup-file-structure)
  (let [command (fetch-command *command-line-args*)
        options (fetch-options *command-line-args*)]
    (run command options)))
